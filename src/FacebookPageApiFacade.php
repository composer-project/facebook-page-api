<?php

namespace ComposerProject\FacebookPageApi;

use Illuminate\Support\Facades\Facade;

/**
 * @see \ComposerProject\FacebookPageApi\Skeleton\SkeletonClass
 */
class FacebookPageApiFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'facebook-page-api';
    }
}
