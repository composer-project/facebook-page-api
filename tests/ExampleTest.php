<?php

namespace ComposerProject\FacebookPageApi\Tests;

use ComposerProject\FacebookPageApi\FacebookPageApi;
use Orchestra\Testbench\TestCase;
use ComposerProject\FacebookPageApi\FacebookPageApiServiceProvider;

class ExampleTest extends TestCase
{

    protected function getPackageProviders($app)
    {
        return [FacebookPageApiServiceProvider::class];
    }

    /** @test */
    public function true_is_true()
    {
        $this->assertTrue(true);
    }

    public function testAuthAccessToken()
    {
        $fb = new FacebookPageApi();
        dd($fb->getAuthAccessToken());
    }
}
