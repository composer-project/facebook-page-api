<?php


namespace ComposerProject\FacebookPageApi;


use Facebook\Exceptions\FacebookSDKException;

class FacebookPageFeed extends FacebookPageApi
{
    private $pageaccesstoken;

    public function __construct()
    {
        parent::__construct();
        $this->pageaccesstoken = $this->getPageAccessToken();
    }

    public function getPageAccessToken()
    {
        $response = $this->client->get('/985326201536930?fields=access_token', 'EAAQIuSgyCCMBAMSXIWIZBr8HM70hj5hnHEpGN97BdZA2ArWuYxrthD21ZCq1PlZBvTl1KqYF2IOZAwdRsM2bkhJbxXTG170Ijwm7ulZCyMoZBTys59oP9qUzUZBrwH8i2hkrID02a0LRDvlR8ri5ZAGnPTaXPt7zyy1khhPZBylq6pEZAhjwTKbZCq4YSscKAtyVIDulFN6n9w2ymXoJZBD4HZB0wjdl2xf3MMtbZBh44ZCwVQEiYgZDZD');

        $re = $response->getGraphNode();
        return $re['access_token'];
    }
    public function getFeed()
    {
        try {
            $response = $this->client->get('/985326201536930/feed');
        } catch (FacebookSDKException $e) {
            return $e->getMessage();
        }

        return json_decode($response->getBody());
    }

    public function postFeedText($message)
    {
        try {
            $response = $this->client->post('/985326201536930/feed', [
                ["message" => $message],
                'EAAQIuSgyCCMBAMSXIWIZBr8HM70hj5hnHEpGN97BdZA2ArWuYxrthD21ZCq1PlZBvTl1KqYF2IOZAwdRsM2bkhJbxXTG170Ijwm7ulZCyMoZBTys59oP9qUzUZBrwH8i2hkrID02a0LRDvlR8ri5ZAGnPTaXPt7zyy1khhPZBylq6pEZAhjwTKbZCq4YSscKAtyVIDulFN6n9w2ymXoJZBD4HZB0wjdl2xf3MMtbZBh44ZCwVQEiYgZDZD'
            ]);
        } catch (FacebookSDKException $e) {
            return $e->getMessage();
        }

        return json_decode($response->getBody());
    }
}
