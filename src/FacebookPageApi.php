<?php

namespace ComposerProject\FacebookPageApi;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Facebook\FacebookResponse;
use Illuminate\Support\Facades\Http;

class FacebookPageApi
{
    protected $client;

    public function __construct()
    {
        try {
            $this->client = new Facebook([
                "app_id" => '1135491243182115',
                "app_secret" => '7612c7b83b35a1d00c805987eafc041f',
                "default_graph_version" => "v6.0",
                "default_access_token" => 'EAAQIuSgyCCMBAEGZCYMQHuHtgxggAMI1XKcTFb6RJxdVYYCFFpc2d73ZBA5oHiR7LGFoIsM69lxXT3H9wbGTvBdSMfKmZA3020NXFZBFNLaiHvZBnZC9rLse8zX8gfPqRt1VgZBjb5ZA6xf0ze3bpe3hR1vINDnwbzzUZAkVNUXQHs54ZCdZAlD6lLLRH5dntaNkOj9yidQQKf9BQZDZD'
            ]);
        } catch (FacebookSDKException $e) {
            return $e->getMessage();
        }
    }

    public function getUser()
    {

        try {
            $response = $this->client->get('/me/accounts', config('config.access_token'));
        }catch (FacebookResponseException $exception) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $exception->getMessage();
            exit;
        }catch (FacebookSDKException $exception) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $exception->getMessage();
            exit;
        }

        try {
            return $response->getBody();
        } catch (FacebookSDKException $e) {
            return $e->getMessage();
        }
    }

    public function getFeed()
    {
        try {
            $response = $this->client->get('/985326201536930/feed', config('config.access_token'));
        } catch (FacebookSDKException $e) {
            return $e->getMessage();
        }

        return json_decode($response->getBody());
    }







}
