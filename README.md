# Very short description of the package

[![Latest Version on Packagist](https://img.shields.io/packagist/v/composer-project/facebook-page-api.svg?style=flat-square)](https://packagist.org/packages/composer-project/facebook-page-api)
[![Build Status](https://img.shields.io/travis/composer-project/facebook-page-api/master.svg?style=flat-square)](https://travis-ci.org/composer-project/facebook-page-api)
[![Quality Score](https://img.shields.io/scrutinizer/g/composer-project/facebook-page-api.svg?style=flat-square)](https://scrutinizer-ci.com/g/composer-project/facebook-page-api)
[![Total Downloads](https://img.shields.io/packagist/dt/composer-project/facebook-page-api.svg?style=flat-square)](https://packagist.org/packages/composer-project/facebook-page-api)

This is where your description should go. Try and limit it to a paragraph or two, and maybe throw in a mention of what PSRs you support to avoid any confusion with users and contributors.

## Installation

You can install the package via composer:

```bash
composer require composer-project/facebook-page-api
```

## Usage

``` php
// Usage description here
```

### Testing

``` bash
composer test
```

### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

### Security

If you discover any security related issues, please email contact@sodec.pro instead of using the issue tracker.

## Credits

- [Sodec](https://github.com/composer-project)
- [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

## Laravel Package Boilerplate

This package was generated using the [Laravel Package Boilerplate](https://laravelpackageboilerplate.com).